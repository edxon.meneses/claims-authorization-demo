﻿using JusticeLeagueAPI.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace JusticeLeagueAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperHeroController : ControllerBase
    {
        //private static List<SuperHero> heros = new List<SuperHero>
        //    {
        //        new SuperHero {
        //            Id = 1,
        //            Name = "Batman",
        //            FirstName = "Bruce",
        //            LastName = "Wayne",
        //            Place = "Gotham City"
        //        },
        //        new SuperHero {
        //            Id = 2,
        //            Name = "Aquaman",
        //            FirstName = "Arthur",
        //            LastName = "Curry",
        //            Place = "Atlantis"
        //        },
        //        new SuperHero
        //        {
        //            Id= 3,
        //            Name = "Superman",
        //            FirstName = "Clark",
        //            LastName = "Kent",
        //            Place = "Metropolis"
        //        }
        //    };
        private readonly DataContext _context;

        public SuperHeroController(DataContext context)
        {
            _context = context;
        }

        [HttpGet, Authorize(Roles = "Admin")]
        [Authorize(policy: "SuperHeroOnly")]
        
        public async Task<ActionResult<List<SuperHero>>> GetAllHeros()
        {

            return Ok(await _context.SuperHero.ToListAsync());
        }

        [HttpGet("{Id}"), Authorize(policy: "SuperHeroOnly")]
        [Authorize(policy: "CapeHeroOnly")]
        public async Task<ActionResult<SuperHero>> GetSingleHero(int Id)
        {
            var hero = await _context.SuperHero.FindAsync(Id);
            if (hero == null)
            {
                return BadRequest("Hero not found");
            }
            return Ok(hero);
        }

        [HttpPost]
        public async Task<ActionResult<List<SuperHero>>> AddHero(SuperHero hero)
        {
            _context.SuperHero.Add(hero);
            await _context.SaveChangesAsync();
            return await GetAllHeros();
        }

        [HttpPut]
        public async Task<ActionResult<List<SuperHero>>> UpdateHero(SuperHero hero)
        {
            var dbhero = await _context.SuperHero.FindAsync(hero.Id);
            if (dbhero == null)
            {
                return BadRequest("Hero not found");
            }
            dbhero.Name = hero.Name;
            dbhero.FirstName = hero.FirstName;
            dbhero.LastName = hero.LastName;
            dbhero.Place = hero.Place;

            await _context.SaveChangesAsync();

            return await GetAllHeros();
        }

        [HttpDelete("{Id}")]
        public async Task<ActionResult<List<SuperHero>>> DeleteSingleHero(int Id)
        {
            var hero = await _context.SuperHero.FindAsync(Id);
            if (hero == null)
            {
                return BadRequest("Hero not found");
            }
            _context.SuperHero.Remove(hero);
            await _context.SaveChangesAsync();
            return await GetAllHeros();
        }
    }
}
